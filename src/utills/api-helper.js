import { create } from "axios";

/**
 * Axios Headers Setting
 */
export const API = create({
    timeout: 60000,
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Client-ID edc51f1c87fd0ee",
    }
});

// export const setAuthHeader = client_id => {
//     API.defaults.headers.common.Authorization = `Client-ID ${client_id}`;
// };
export const EndPoints = {
    GallerySearch: `https://api.imgur.com/3/gallery/search/top/week/0`
}

export default {
    API,
    EndPoints
};
