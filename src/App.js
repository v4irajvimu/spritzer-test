import React from 'react';
import { connect } from 'react-redux'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import { Container, } from 'react-bootstrap'
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Header from './components/Header';
import SearchBar from './components/SearchBar';
import { getDataForReactTable } from './redux/selectors/images/imagesSelector';
import ToggleResultsCheckBox from './components/ToggleResultsCheckBox';


function App(props) {
  const columns = [
    {
      Header: "",
      accessor: 'img_src',
      style: {
        textAlign: "left",
        alignSelf: "center"
      },
      headerClassName: "text-uppercase  text-left",
      Cell: ({ original }) =>
        <div className="" style={{ width: "100px", height: "100px" }}>
          <img
            src={`${original.img_src}`}
            alt=""
            style={{ width: "100%", height: "100%" }}
          />
        </div>
    },
    {
      Header: "Title",
      accessor: 'title',
      style: {
        textAlign: "left",
        alignSelf: "center"
      },
      headerClassName: "text-uppercase  text-left"
    },
    {
      Header: "Date Time",
      accessor: 'datetime_display',
      style: {
        textAlign: "right",
        alignSelf: "center"
      },
      headerClassName: "text-uppercase  text-right"
    },
    {
      Header: "Additional Images",
      accessor: 'additional_imgs',
      style: {
        textAlign: "right",
        alignSelf: "center"
      },
      headerClassName: "text-uppercase  text-right",
      Cell: ({ original }) => original.additional_imgs > 0 ?
        <span>{original.additional_imgs}</span> : "-"
    },
  ];
  return (
    <Container>
      <Header />
      <SearchBar />
      {props.data.length > 0 && (
        <>
          <ToggleResultsCheckBox />
          {/* React Table Goes Here. */}
          <div className="row">
            <div className="col-sm-12">
              <ReactTable
                data={props.data}
                columns={columns}
                defaultPageSize={25}
                minRows={0}
                defaultSorted={[
                  {
                    id: "datetime_display",
                    desc: true
                  }
                ]}
                className="-striped -highlight"
                style={{ backgroundColor: "#fff" }}
              />
            </div>
          </div>
        </>
      )}
      <NotificationContainer />
    </Container>
  );
}

const mapStateToProps = state => ({
  data: getDataForReactTable(state.images.list, state.filters),
  toggle_checked: state.filters.toggle_checked
})

export default connect(mapStateToProps)(App);
