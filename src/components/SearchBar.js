import React from 'react'
import { connect } from 'react-redux'
import { termOnChange } from '../redux/actions/filters/filtersActionCreator'
import { getImagesFromApi } from '../redux/actions/images/imagesActionCreator'

function SearchBar(props) {
    const handleChange = e => {
        props.termOnChange(e.target.value)
    }
    return (
        <div className="row">
            <div className="col-sm-8 offset-sm-2">
                <form className=""
                    onSubmit={(e) => {
                        e.preventDefault()
                        props.getImagesFromApi(props.term)
                    }}
                >
                    <div className="form-row align-items-center">
                        <div className="col-10">
                            <label className="sr-only" htmlFor="inlineFormInput">Name</label>
                            <input
                                type="text"
                                className="form-control mb-2"
                                id="inlineFormInput"
                                placeholder="Type search term.."
                                value={props.term}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="col-2">
                            <button type="submit" className="btn btn-primary mb-2" > Search </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
const mapStateToProps = state => (
    {
        term: state.filters.term
    }
)
export default connect(
    mapStateToProps,
    {
        termOnChange,
        getImagesFromApi
    }
)(SearchBar);
