import React from 'react'
import { connect } from 'react-redux'
import { Form } from 'react-bootstrap'
import { toggleResults } from '../redux/actions/filters/filtersActionCreator'

function ToggleResultsCheckBox(props) {
    return (
        <div className="row">
            <div className="col-sm-12">
                <Form.Check
                    checked={props.toggle_checked}
                    label={"Toggle Results."}
                    onChange={() => props.toggleResults()}
                    style={{ color: "#fff" }}
                />
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    toggle_checked: state.filters.toggle_checked
})

export default connect(mapStateToProps, { toggleResults })(ToggleResultsCheckBox) 
