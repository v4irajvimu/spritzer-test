import React from 'react'
import logoSrc from '../resources/imgur-log.png'
export default function Header() {
    return (
        <div className="row mt-3 ">
            <div className="col-sm-12 text-center">
                <img src={logoSrc} alt="" style={{ width: "20%" }} />
            </div>
            <div className="col-sm-12 text-center text-uppercase">
                <h1 style={{ color: "#fff" }}>top images of the week</h1>
            </div>
        </div>
    )
}
