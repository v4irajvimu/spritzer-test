import { SEARCH_TERM_ON_CHANGE, TOGGLE_RESULTS } from "./filtersActionTypes"

/**
 * Search bar input field onchange event state change.
 * @param {String} term Search term
 */
export const termOnChange = term => dispatch => {
    dispatch({
        type: SEARCH_TERM_ON_CHANGE,
        payload: term
    })
}

/**
 * Toggle between results filtration.
 */
export const toggleResults = () => dispatch => {
    dispatch({
        type: TOGGLE_RESULTS
    })
}