import { API, EndPoints } from "../../../utills/api-helper"
import { STORE_IMAGES_FROM_API } from "./imagesActionTypes"
import { NotificationManager } from 'react-notifications';

/**
 * Get all images by search term.
 * @param {String} term Search Term
 */
export const getImagesFromApi = (term = "") => dispatch => {
    const url = `${EndPoints.GallerySearch}?q=${term}&q_type=jpeg&q_type=png`
    API.get(url).then(res => {
        if (res.status === 200 && res.data && res.data.data) {
            dispatch({
                type: STORE_IMAGES_FROM_API,
                payload: res.data.data || []
            })

            if (res.data.data.length === 0) {
                NotificationManager.error("Try with different search term", "No results found...! ")
            }
        }

    })
}