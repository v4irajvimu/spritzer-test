import moment from 'moment-timezone'

/**
 * Prepare data list for the react table and apply filters if have any.
 * @param {Array} img_list Array of images objects received as response
 * @param {Object} filters Filter Object
 */
export const getDataForReactTable = (img_list = [], filters = {}) => {
    // return empty array if there is no images,
    if (img_list.length === 0) return [];

    let mappedList = img_list.map(imgObj => {
        const images = imgObj.images;

        // Calculation for toggle results.
        const weirdTotal = Number(imgObj.points) + Number(imgObj.score) + Number(imgObj.topic_id);
        let link = null;
        let additional_imgs = 0;

        // Multiple Images included.
        if (images && images.length > 0) {

            link = images[0].link
            additional_imgs = images.length - 1;
        }
        // Single Image Object
        else {
            link = imgObj.link
        }



        return {
            title: imgObj.title,
            datetime: imgObj.datetime,
            // Converting the timestamp to date time format
            datetime_display: moment.unix(imgObj.datetime).tz("Asia/Colombo").format("DD/MM/YYYY h:mm a"),
            img_src: link,
            additional_imgs,
            is_even: weirdTotal % 2 === 0
        }
    })

    // ! Filters Goes Here
    if (filters.toggle_checked) {
        mappedList = mappedList.filter(img => img.is_even)
    }

    return mappedList;
}