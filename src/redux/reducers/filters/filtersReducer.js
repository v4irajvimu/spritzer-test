import { SEARCH_TERM_ON_CHANGE, TOGGLE_RESULTS } from "../../actions/filters/filtersActionTypes";

const filtersInitialState = {
    term: "",
    toggle_checked: false
}

export default (state = filtersInitialState, { type, payload }) => {
    switch (type) {
        case SEARCH_TERM_ON_CHANGE: {
            return {
                ...state,
                term: payload
            }
        }
        case TOGGLE_RESULTS: {
            return {
                ...state,
                toggle_checked: !state.toggle_checked
            }
        }

        default:
            return state;
    }
}