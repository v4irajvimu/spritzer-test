import { combineReducers } from "redux";
import imagesReducer from "./images/imagesReducer";
import filtersReducer from "./filters/filtersReducer";

export default combineReducers({
    images: imagesReducer,
    filters: filtersReducer,
});
