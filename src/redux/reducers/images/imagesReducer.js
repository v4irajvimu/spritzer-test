import { STORE_IMAGES_FROM_API } from "../../actions/images/imagesActionTypes";

const imagesInitialState = {
    list: []
}

export default (state = imagesInitialState, { type, payload }) => {
    switch (type) {
        case STORE_IMAGES_FROM_API: {
            return {
                ...state,
                list: payload.length > 0 ? payload : []
            }
        }
        default:
            return state;
    }
}