## Clone project form gitlab repository below.

- https://gitlab.com/v4irajvimu/spritzer-test
- run `npm install` from project root to download all project dependencies

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

## License

MIT © [v4irajvimu](https://gitlab.com/v4irajvimu)
